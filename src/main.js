import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './plugins/router';
import VueProgressBar from 'vue-progressbar'

Vue.config.productionTip = false


require("../node_modules/bootstrap/dist/css/bootstrap.min.css");
Vue.prototype.url = "https://smdps.herokuapp.com"

Vue.use(VueProgressBar, {
  color: 'green',
  failedColor: 'red',
  height: '100'
})


new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
